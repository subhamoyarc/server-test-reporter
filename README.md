# ARC NodeJS Test Reporter

## Description

This is a simple package for reporting NodeJS Tests.

## Installation

```sh

  npm install @himadrim/server-test-reporter @jest/reporters --save

```

## Configuration

Add the below configuration to the reporters property of your jest configuration

```json
"jest": {
  ...,
  "reporters": [
    "default",
    [
      "@himadrim/server-test-reporter",
      {
        "to": ["example@test.com"],
        "messageSubject": "Test Results"
      }
    ]
  ]
  ...
}
```

If it fails, replace the name of package with the location of the exact file
```json
"jest": {
  ...,
  "reporters": [
    "default",
    [
      "<rootDir>/node_modules/@himadrim/server-test-reporter/lib/email-reporter.js",
      {
        "to": ["example@test.com"],
        "messageSubject": "Test Results"
      }
    ]
  ]
  ...
}
```

## Options

| Option              | Type      | Default       | Description                  |
|:--------------------|:----------|:--------------|:-----------------------------|
| to                  | string[ ] | [ ]           | array of receipent email ids |
| messageSubject      | string    | 'Test Report' | custom subject of the email  |
| reportOnlyOnFailure | boolean   | false         | no email on success          |

## Development
- Create a `feature/bugfix/hotfix` branch from `dev` branch and send a consolidated pull request to the `dev` branch
- For publishing the package, checkout the `dev` branch and run `npm version <type>`. { type: major/minor/patch }
- Send a pull request from the `dev` branch to the `release` branch.
- Once the pull request is merged, the bitbucket pipeline will automatically publish the package into the `npm registry`.

## References
- [Jest Reporters Configuration](https://jestjs.io/docs/configuration#reporters-arraymodulename--modulename-options)
- [Jest Reporters NotifyReporter](https://github.com/facebook/jest/blob/main/packages/jest-reporters/src/NotifyReporter.ts)

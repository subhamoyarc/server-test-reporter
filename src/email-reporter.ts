import type { AggregatedResult, TestResult, AssertionResult } from '@jest/test-result';
import type { Config, Context, Reporter } from '@jest/reporters';
import { BaseReporter, utils } from '@jest/reporters';
import { validateOrReject, ValidationError } from 'class-validator';
import { plainToClass } from 'class-transformer';
import { Utilities } from '@himadrim/server-util';
import { logger, defaultOptions } from './config';
import { Options } from './interface';
import Convert from 'ansi-to-html';
import { basename } from 'path';

export function formatAssertionResult(res: AssertionResult) {
  return [
    res.fullName,
    ...res.failureMessages.map((message) => new Convert().toHtml(message)),
  ].join('\n');
}

export function formatTestResult(res: TestResult, index = 1) {
  const fileName = basename(res.testFilePath);
  const details = res.testResults
    .filter((assertion) => assertion.failureMessages.length)
    .map((assertion, j) => `${index}.${j + 1}. ${formatAssertionResult(assertion)}`)
    .join('\n');

  return `File: ${fileName}\n${details}\n`;
}

export function formatAggregateResult(res: AggregatedResult) {
  return res.testResults
    .filter((test) => test.numFailingTests)
    .map((test, i) => formatTestResult(test, i + 1))
    .join('\n');
}

export default class EmailReporter extends BaseReporter implements Reporter {
  protected _globalConfig: Config.GlobalConfig;
  protected _options: Options;
  protected _convertAnsi: Convert;

  constructor(globalConfig: Config.GlobalConfig, options: Options) {
    super();
    this._globalConfig = globalConfig;
    this._options = plainToClass(Options, { ...defaultOptions, ...options });
    this._convertAnsi = new Convert();
    validateOrReject(this._options).catch((errors) => {
      throw new Error(
        errors
          .map(
            ({ property, constraints }: ValidationError) =>
              `Options:"${property}" -> ${
                constraints ? Object.values<string>(constraints).join(',') : ''
              }`,
          )
          .join('\n'),
      );
    });
  }

  onRunComplete(_contexts?: Set<Context>, result?: AggregatedResult): Promise<any> {
    try {
      if (!result) {
        return Promise.resolve({ message: 'No Result' });
      }
      const success = result.numFailedTests === 0 && result.numRuntimeErrorTestSuites === 0;
      const message: string = success ? '' : formatAggregateResult(result);

      const body = [
        success ? 'No Tests Failed' : 'Some Tests Failed',
        'Tests Summary:',
        '--------------',
        this._convertAnsi.toHtml(utils.getSummary(result)),
        '--------------\n',
        message,
      ].join('\n');

      if (this._options.reportOnlyOnFailure && success) {
        return Promise.resolve({});
      }
      if (this._options.to.length === 0) {
        logger.info('No receipents provided');
        return Promise.resolve({});
      }
      return Utilities.sendMail({
        toRecepient: this._options.to,
        messageSubject:
          this._options.messageSubject +
          (this._options.envName && process.env[this._options.envName]
            ? ' : ' + process.env[this._options.envName]?.toLocaleUpperCase()
            : ''),
        messageBody: '<pre>' + body.replace(/\n/g, '<br />') + '<pre>',
      });
    } catch (e) {
      logger.error(typeof e === 'object' ? JSON.stringify(e) : e);
      return Promise.reject(e);
    }
  }
}

import { IsEmail, IsString, IsBoolean, IsOptional } from 'class-validator';

export class Options {
  @IsEmail({}, { each: true })
  to!: string[];

  @IsString()
  @IsOptional()
  messageSubject?: string;

  @IsBoolean()
  @IsOptional()
  reportOnlyOnFailure?: boolean;

  @IsString()
  @IsOptional()
  envName?: string;
}

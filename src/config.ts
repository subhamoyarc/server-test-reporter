import { ArcLogger, ArcLoggerBase, LogLevels } from '@himadrim/server-logger';
import { Options } from './interface';

const arcLogger: ArcLogger = new ArcLogger({
  title: 'Server-Test-Reporter',
  level: LogLevels.Silly,
});

export const logger: ArcLoggerBase = arcLogger.getLogger();
export const defaultOptions: Options = {
  to: [],
  messageSubject: 'Test Results',
  reportOnlyOnFailure: false,
  envName: 'TEST_ENV',
};

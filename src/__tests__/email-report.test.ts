import { Utilities } from '@himadrim/server-util';
import { Options } from '../interface';
import { defaultOptions } from '../config';
import EmailReporter, {
  formatAggregateResult,
  formatTestResult,
  formatAssertionResult,
} from '../email-reporter';
import * as passingResult from './data/passing.json';
import * as failingResult from './data/failing.json';
import * as failingMultipleResult from './data/failing-multiple.json';

beforeAll(() => {
  process.env.TEST_ENV = 'test';
});

describe('Results Formatter', () => {
  test('empty string for no failures', () => {
    expect(formatAggregateResult(passingResult as any)).toBe('');
  });

  test('proper string for failure', () => {
    expect(
      formatAggregateResult(failingResult as any)
        .split('\n')
        .slice(0, 2),
    ).toStrictEqual(['File: sync.test.ts', '1.1. Sync (case: Owner) get folder permission']);
  });

  test('format test result', () => {
    expect(
      formatTestResult(failingMultipleResult.testResults[1] as any, 2)
        .split('\n')
        .slice(0, 7),
    ).toStrictEqual([
      'File: attachment.test.js',
      '2.1. insert document',
      'Error: connect ECONNREFUSED 127.0.0.1:3000',
      '    at TCPConnectWrap.afterConnect [as oncomplete] (net.js:1159:16)',
      '2.2. insert image',
      'Error: connect ECONNREFUSED 127.0.0.1:3000',
      '    at TCPConnectWrap.afterConnect [as oncomplete] (net.js:1159:16)',
    ]);
  });

  test('format assertion result', () => {
    expect(
      formatAssertionResult(failingMultipleResult.testResults[1].testResults[0] as any).split('\n'),
    ).toStrictEqual([
      'insert document',
      'Error: connect ECONNREFUSED 127.0.0.1:3000',
      '    at TCPConnectWrap.afterConnect [as oncomplete] (net.js:1159:16)',
    ]);
  });
});

describe('JestEmailReporter', () => {
  let reporter: EmailReporter;
  let reporterOnOnlyFailure: EmailReporter;
  let mailMock: jest.SpyInstance<Utilities>;
  const options: Options = {
    ...defaultOptions,
    to: ['lokesh.mohanty@e-arc.com'],
    messageSubject: 'Test Results',
  };

  beforeAll(() => {
    mailMock = process.env.TEST_PUBLISH
      ? jest.spyOn(Utilities, 'sendMail').mockImplementation(
          jest.fn(async () => {
            return { MessageUniqueId: '', message: 'success' };
          }),
        )
      : jest.spyOn(Utilities, 'sendMail');
    reporter = new EmailReporter({} as any, options);
    reporterOnOnlyFailure = new EmailReporter({} as any, { ...options, reportOnlyOnFailure: true });
  });

  beforeEach(() => {
    mailMock.mockClear();
  });

  test('email report after successful test', async () => {
    expect.hasAssertions();
    await expect(reporter.onRunComplete(undefined, passingResult as any)).resolves.toStrictEqual({
      MessageUniqueId: expect.any(String),
      message: 'success',
    });
    expect(mailMock).toHaveBeenCalledTimes(1);
    expect(mailMock).toHaveBeenCalledWith({
      toRecepient: options.to,
      messageSubject: options.messageSubject + ' : TEST',
      messageBody: expect.any(String),
    });
    expect(mailMock.mock.results[0].value).resolves.toStrictEqual({
      MessageUniqueId: expect.any(String),
      message: 'success',
    });
  });

  test('email report after failed test', async () => {
    expect.hasAssertions();
    await expect(reporter.onRunComplete(undefined, failingResult as any)).resolves.toStrictEqual({
      MessageUniqueId: expect.any(String),
      message: 'success',
    });
    expect(mailMock).toHaveBeenCalledTimes(1);
    expect(mailMock).toHaveBeenCalledWith({
      toRecepient: options.to,
      messageSubject: options.messageSubject + ' : TEST',
      messageBody: expect.any(String),
    });
    expect(mailMock.mock.results[0].value).resolves.toStrictEqual({
      MessageUniqueId: expect.any(String),
      message: 'success',
    });
  });

  test('no email report after successful test if reportOnOnlyFailure', async () => {
    expect.hasAssertions();
    await expect(
      reporterOnOnlyFailure.onRunComplete(undefined, passingResult as any),
    ).resolves.toStrictEqual({});
    expect(mailMock).toHaveBeenCalledTimes(0);
  });

  test('indexing in email report after failed test', async () => {
    expect.hasAssertions();
    await expect(
      reporter.onRunComplete(undefined, failingMultipleResult as any),
    ).resolves.toStrictEqual({
      MessageUniqueId: expect.any(String),
      message: 'success',
    });
    expect(mailMock).toHaveBeenCalledTimes(1);
    expect(mailMock).toHaveBeenCalledWith({
      toRecepient: options.to,
      messageSubject: options.messageSubject + ' : TEST',
      messageBody: expect.any(String),
    });
    expect(mailMock.mock.results[0].value).resolves.toStrictEqual({
      MessageUniqueId: expect.any(String),
      message: 'success',
    });
  });
});

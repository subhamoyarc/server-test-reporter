import EmailReporter from './email-reporter';

export type { Options } from './interface';
export default EmailReporter;
